using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelecaoPersonagem : MonoBehaviour
{

    public Image fotoHumano, fotoZombie;
    public List<Sprite> skinsHumanos, skinsZombies;

    private int indexH, indexZ;

    // Start is called before the first frame update
    void Start()
    {
        indexH = PlayerPrefs.GetInt("skinhumano");
        indexZ = PlayerPrefs.GetInt("skinzombie");
        fotoHumano.sprite = skinsHumanos[indexH];
        fotoZombie.sprite = skinsZombies[indexZ];
    }

    public void rightSelectionHumanos()
    {
        indexH++;
        if (indexH >= skinsHumanos.Count) indexH = 0;
        fotoHumano.sprite = skinsHumanos[indexH];
        PlayerPrefs.SetInt("skinhumano", indexH);
    }

    public void leftSelectionHumanos()
    {
        indexH--;
        if (indexH < 0) indexH = skinsHumanos.Count-1;
        fotoHumano.sprite = skinsHumanos[indexH];
        PlayerPrefs.SetInt("skinhumano", indexH);
    }

    public void rightSelectionZombies()
    {
        indexZ++;
        if (indexZ >= skinsZombies.Count) indexZ = 0;
        fotoZombie.sprite = skinsZombies[indexZ];
        PlayerPrefs.SetInt("skinzombie", indexZ);
    }

    public void leftSelectionZombies()
    {
        indexZ--;
        if (indexZ < 0) indexZ = skinsZombies.Count - 1;
        fotoZombie.sprite = skinsZombies[indexZ];
        PlayerPrefs.SetInt("skinzombie", indexZ);
    }

}
