﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class MenuManager : MonoBehaviourPunCallbacks
{
	public static MenuManager Instance;
	private Launcher laucher;
	public GameObject btFindRoom, btCriarSala, btStartarGameOff;


	[SerializeField] Menu[] menus;

	void Awake()
	{
		Instance = this;
        if (PhotonNetwork.OfflineMode)
        {
			foreach(Menu menu in menus)
            {
				if (menu !=null && menu.menuName == "title") OpenMenu(menu);
				if (menu != null && menu.menuName == "loading") CloseMenu(menu);
            }
        }
		laucher = GetComponent<Launcher>();
	}

    private void FixedUpdate()
    {
		RendererBotoes();
	}

    public void OpenMenu(string menuName)
	{
		for(int i = 0; i < menus.Length; i++)
		{
			if(menus[i].menuName == menuName)
			{
				if(!PhotonNetwork.OfflineMode) menus[i].Open();
			}
			else if(menus[i].open)
			{
				CloseMenu(menus[i]);
			}
		}
	}

	public void OpenMenu(Menu menu)
	{
		for(int i = 0; i < menus.Length; i++)
		{
			if(menus[i].open)
			{
				CloseMenu(menus[i]);
			}
		}
		if (menu.menuName == "create room" && PhotonNetwork.OfflineMode)
		{
			menu.Close();
			laucher.CreateRoom();
		}
		else
		{
			menu.Open();
		}
	}

	public void CloseMenu(Menu menu)
	{
		menu.Close();
	}

	public void QuitGame()
    {
		Application.Quit();
    }

	private void RendererBotoes()
    {
        if (PhotonNetwork.OfflineMode)
        {
			if (btFindRoom != null) btFindRoom.SetActive(false);
			if (btCriarSala != null) btCriarSala.SetActive(false);
			if (btStartarGameOff != null) btStartarGameOff.SetActive(true);
		}
        else
        {
			if (btFindRoom != null) btFindRoom.SetActive(true);
			if (btCriarSala != null) btCriarSala.SetActive(true);
			if (btStartarGameOff != null) btStartarGameOff.SetActive(false);
		}
    }

}