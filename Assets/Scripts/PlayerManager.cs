using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PlayerManager : MonoBehaviourPunCallbacks
{
	PhotonView PV;

	void Awake()
	{
		PV = GetComponent<PhotonView>();
	}

	void Start()
	{
		if(PV.IsMine)
		{
			CreateController();
		}
	}

    public void CreateController()
	{
		if (PlayerPrefs.GetInt("skinhumano") == 0)
        {
			PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerHumano0"), Vector3.zero, new Quaternion(0, 0, 0, 0), 0, new object[] { PV.ViewID });
        }
        else if (PlayerPrefs.GetInt("skinhumano") == 1)
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerHumano1"), Vector3.zero, new Quaternion(0, 0, 0, 0), 0, new object[] { PV.ViewID });
        }
        else 
        {
			PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerHumano0"), Vector3.zero, new Quaternion(0, 0, 0, 0), 0, new object[] { PV.ViewID });
		}
		Destroy(this.gameObject);
	}

	
}
