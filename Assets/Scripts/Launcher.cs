using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Launcher : MonoBehaviourPunCallbacks
{
	public static Launcher Instance;

	[SerializeField] TMP_InputField roomNameInputField;
	[SerializeField] TMP_InputField roomPasswordInputField;
	[SerializeField] TMP_InputField roomNameFiltro;
	[SerializeField] TMP_Text errorText;
	[SerializeField] TMP_Text roomNameText;
	[SerializeField] Transform roomListContent;
	[SerializeField] GameObject roomListItemPrefab;
	[SerializeField] Transform playerListContent;
	[SerializeField] GameObject PlayerListItemPrefab;
	[SerializeField] GameObject startGameButton;
	private List<RoomInfo> roomListCache;
	public bool isSalaPrivada;
	private bool isStartou = false;
	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		Debug.Log("Connecting to Master - Offline mode: "+ PhotonNetwork.OfflineMode);
		if (!PhotonNetwork.IsConnected && !PhotonNetwork.OfflineMode)
		{
			Debug.Log("Connecting to Master");
			PhotonNetwork.ConnectUsingSettings();
		}
		isStartou = false;
	}

	public override void OnConnectedToMaster()
	{
		Debug.Log("Connected to Master2");
		if(!PhotonNetwork.OfflineMode) PhotonNetwork.JoinLobby();
		PhotonNetwork.AutomaticallySyncScene = true;
	}

	public override void OnJoinedLobby()
	{
		MenuManager.Instance.OpenMenu("title");
		Debug.Log("Joined Lobby");
	}

	public void CreateRoom()
	{
		PhotonNetwork.NickName = PlayerPrefs.GetString("nickname") != null && PlayerPrefs.GetString("nickname") != "" ? PlayerPrefs.GetString("nickname") : "Player " + Random.Range(0, 1000).ToString("0000");
		if (string.IsNullOrEmpty(roomNameInputField.text))
		{
			roomNameInputField.text = PhotonNetwork.NickName;
		}
		string nomeSala = roomNameInputField.text+ "#.#.#" + roomPasswordInputField.text;

		int posicao = nomeSala.IndexOf("#.#.#");
		string password = nomeSala.Substring(posicao+5, Mathf.Abs(posicao+5-nomeSala.Length));

		Debug.Log("Password: "+ password);

		PhotonNetwork.CreateRoom(nomeSala);
		if (!PhotonNetwork.OfflineMode)
		{
			MenuManager.Instance.OpenMenu("loading");
		}
		else
		{
			StartGame();
		}
	}

	public override void OnJoinedRoom()
	{
		if (SceneManager.GetActiveScene().buildIndex != 1) return;
		PhotonNetwork.NickName = PlayerPrefs.GetString("nickname") != null && PlayerPrefs.GetString("nickname") != "" ? PlayerPrefs.GetString("nickname") : "Player " + Random.Range(0, 1000).ToString("0000");
		MenuManager.Instance.OpenMenu("room");
		int posicao = PhotonNetwork.CurrentRoom.Name.IndexOf("#.#.#");
		string nomeSala = PhotonNetwork.CurrentRoom.Name.Substring(0, posicao);
		roomNameText.text = nomeSala;

		Player[] players = PhotonNetwork.PlayerList;

        if (playerListContent != null)
        {
			foreach (Transform child in playerListContent)
			{
				Destroy(child.gameObject);
			}

			if (PlayerListItemPrefab != null)
			{
				for (int i = 0; i < players.Count(); i++)
				{
					Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(players[i]);
				}
			}
		}
        Debug.Log("Players max: "+PhotonNetwork.CurrentRoom.MaxPlayers);
		if (startGameButton != null) startGameButton.SetActive(PhotonNetwork.IsMasterClient);
	}

	public override void OnMasterClientSwitched(Player newMasterClient)
	{
		if(startGameButton!=null) startGameButton.SetActive(PhotonNetwork.IsMasterClient);
	}

	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		errorText.text = "Room Creation Failed: " + message;
		Debug.LogError("Room Creation Failed: " + message);
		MenuManager.Instance.OpenMenu("error");
	}

	public void StartGame()
	{
		if (isStartou) return;
		MenuManager.Instance.OpenMenu("loading");
		isStartou = true;
		PhotonNetwork.LoadLevel(2);
	}

	public void LeaveRoom()
	{
		Destroy(GameObject.Find("RoomManager"));
		PhotonNetwork.LeaveRoom();
		if (!PhotonNetwork.OfflineMode) MenuManager.Instance.OpenMenu("loading");
	}

	public void JoinRoom(RoomInfo info)
	{
		PhotonNetwork.JoinRoom(info.Name);
		if (!PhotonNetwork.OfflineMode) MenuManager.Instance.OpenMenu("loading");
	}

	public override void OnLeftRoom()
	{
		Debug.Log("Saiu da room");
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (player.GetComponent<PhotonView>().IsMine)
			{
				PhotonNetwork.Destroy(player);
			}
		}
		PhotonNetwork.LoadLevel(1);
		base.OnLeftRoom();
	}

    public override void OnLeftLobby()
    {
		Debug.Log("Saiu do Lobby");
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (player.GetComponent<PhotonView>().IsMine)
			{
				PhotonNetwork.Destroy(player);
			}
		}
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.Disconnect();
		}
		PhotonNetwork.LoadLevel(0);
		base.OnLeftLobby();
    }
	

	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		foreach(Transform trans in roomListContent)
		{
			Destroy(trans.gameObject);
		}

		for(int i = 0; i < roomList.Count; i++)
		{
			if(roomList[i].RemovedFromList)
				continue;
			
			Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomList[i]);
		}
		roomListCache = roomList;
	}

	public void filtrarRoomList()
    {
		foreach (Transform trans in roomListContent)
		{
			Destroy(trans.gameObject);
		}

		for (int i=0; i < roomListCache.Count; i++)
        {
			if (roomNameFiltro.text == null || roomNameFiltro.text == "" || roomListCache[i].Name.Contains(roomNameFiltro.text)) //filtro sala por nome
			{
				Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().SetUp(roomListCache[i]);
			}
		}
	}

	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		if(PlayerListItemPrefab!=null && playerListContent!=null) Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(newPlayer);
	}

	public void VoltarAoMenuPrincipal()
	{
		if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }
		PhotonNetwork.LoadLevel(0);
	}

}
