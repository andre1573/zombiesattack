using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class MenuEndGame : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update

    public void VoltarProLobby() //tem q ser voltar pro menu
    {
        Debug.Log("Voltando ao lobby");
        PhotonNetwork.DestroyAll(false);
        PhotonNetwork.LoadLevel(1);
    }
}
