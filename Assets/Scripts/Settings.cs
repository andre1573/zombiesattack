using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class Settings : MonoBehaviour
{

    public Toggle fullScreenToggle;
    public Slider sensivitySlider;
    public AudioMixer audioMixer, audioMixerVoiceChat;
    Resolution[] resolutions;
    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;
    public TMP_Dropdown idiomaDropdown;
    public Slider volumeSlider, volumeVoiceChatSlider;

    // Start is called before the first frame update
    void Start()
    {
        sensivitySlider.value = PlayerPrefs.GetFloat("sensivity") > 0 ? PlayerPrefs.GetFloat("sensivity") : 2.0f;
        volumeSlider.value = PlayerPrefs.GetFloat("volume");
        volumeVoiceChatSlider.value = PlayerPrefs.GetFloat("volumeVoiceChat");
        qualityDropdown.value = PlayerPrefs.GetInt("graphicsIndex")-1;
        idiomaDropdown.value = PlayerPrefs.GetInt("idiomaIndex");
        fullScreenToggle.isOn = Screen.fullScreen;

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            if(!options.Contains(option)) options.Add(option);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        //

    }

    public void setResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void setFullscren(bool boolean)
    {
        Screen.fullScreen = boolean;
        PlayerPrefs.SetInt("fullscreen", boolean?0:1); //0 = true
        Debug.Log("Setou grafico: " + boolean);
    }

    public void setVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volume", volume);
        volumeSlider.value = volume;
    }

    public void setVolumeVoiceChat(float volume)
    {
        audioMixerVoiceChat.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volumeVoiceChat", volume);
        volumeVoiceChatSlider.value = volume;
    }

    public void setQuality(int index)
    {
        QualitySettings.SetQualityLevel(index+1);
        PlayerPrefs.SetInt("graphicsIndex", index+1);
    }

    public void setSensivity(float value)
    {
        PlayerPrefs.SetFloat("sensivity", value);
    }

    public void setIdioma(int value)
    {
        PlayerPrefs.SetInt("idiomaIndex", value);
    }

}
