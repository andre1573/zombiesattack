using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GrabObjects : MonoBehaviourPunCallbacks
{
    
    private string tagObjeto = "objeto";

    [Tooltip("Force to apply in object")]
    public float forceGrab = 5;
    public float maxDistPlayer, maxDistanceObjeto;
    [Tooltip("Put all layers, the player layer not!")]
    public LayerMask acceptLayers = 0;


    [HideInInspector]
    public GameObject grabedObj;
    [HideInInspector]
    public bool possibleGrab = false;
    private Vector2 rigSaveGrabed;

    [SerializeField] Camera cam;
    [HideInInspector] [SerializeField] PlayerController controller;

    PhotonView PV;

    void Awake()
    {
        PV = GetComponentInParent<PhotonView>();
        controller = GetComponentInParent<PlayerController>();
    }

    void Update()
    {
        if (!controller.canMove) return;
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        ray.origin = cam.transform.position;

        if (Physics.Raycast(ray, out RaycastHit hit, maxDistanceObjeto))
        {
            possibleGrab = false;
            if (hit.transform.tag == tagObjeto)
            {
                if (Input.GetButton("Fire3"))
                {
                    if (hit.transform.gameObject.GetComponent<PhotonView>().Owner == PhotonNetwork.LocalPlayer)
                    {
                        grabedObj = hit.transform.gameObject;
                    }
                    else
                    {
                        hit.transform.gameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
                        grabedObj = hit.transform.gameObject;
                    }
                }
                possibleGrab = true;
            }
        }
        else
        {
            possibleGrab = false;
        }

        if (grabedObj != null)
        {
            if (!grabedObj.GetComponent<Rigidbody>())
            {
                Debug.LogError("Coloque um Rigidbody no objeto!");
                return;
            }

            Rigidbody objRig = grabedObj.GetComponent<Rigidbody>();
            Vector3 posGrab = cam.transform.position + cam.transform.forward * maxDistPlayer;
            float dist = Vector3.Distance(grabedObj.transform.position, posGrab);
            float calc = forceGrab * dist * 6 * Time.deltaTime;

            if (rigSaveGrabed == Vector2.zero)
                rigSaveGrabed = new Vector2(objRig.drag, objRig.angularDrag);
            objRig.drag = 2.5f;
            objRig.angularDrag = 2.5f;
            objRig.AddForce(-(grabedObj.transform.position - posGrab).normalized * calc, ForceMode.Impulse);

            if (Input.GetMouseButtonUp(0)) UngrabObject();

            if (objRig.velocity.magnitude >= 20) UngrabObject();

            if (dist >= 10) UngrabObject();

        }
    }

    void UngrabObject()
    {
        Rigidbody objRig = grabedObj.GetComponent<Rigidbody>();
        objRig.drag = rigSaveGrabed.x;
        objRig.angularDrag = rigSaveGrabed.y;
        rigSaveGrabed = Vector2.zero;
        grabedObj = null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Transform cam = GetComponent<Camera>().transform;
        if (!Physics.Raycast(cam.position, cam.forward, maxDistPlayer))
        {
            Gizmos.DrawLine(cam.position, cam.position + cam.forward * maxDistPlayer);
        }
    }
}
