using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarrasMeleeGun : Gun
{

	[SerializeField] Camera cam;
	public GameObject bulletImpactPrefab;
	public float distanciaMax = 1;

	PhotonView PV;

	void Awake()
	{
		PV = GetComponent<PhotonView>();
	}

	public override void Use()
	{
		Bater();
	}

	void Bater()
	{
		Debug.Log("Atirou");
		Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
		ray.origin = cam.transform.position;
		if (Physics.Raycast(ray, out RaycastHit hit, distanciaMax))
		{
			if (hit.collider.gameObject.GetComponent<ColliderPlayer>() != null && hit.collider.gameObject.GetComponent<ColliderPlayer>().playerController.GetComponent<IDamageable>() != null)
			{
				hit.collider.gameObject.GetComponent<ColliderPlayer>().playerController.GetComponent<IDamageable>().TakeDamage(((GunInfo)itemInfo).damage);
			}
			PV.RPC("RPC_Bater", RpcTarget.All, hit.point, hit.normal);
			Debug.Log("Colidiu");
		}
	}

	[PunRPC]
	void RPC_Bater(Vector3 hitPosition, Vector3 hitNormal)
	{
		Collider[] colliders = Physics.OverlapSphere(hitPosition, 0.3f);
		if (colliders.Length != 0)
		{
			GameObject bulletImpactObj = Instantiate(bulletImpactPrefab, hitPosition + hitNormal * 0.001f, Quaternion.LookRotation(hitNormal, Vector3.up) * bulletImpactPrefab.transform.rotation);
			Destroy(bulletImpactObj, 10f);
			bulletImpactObj.transform.SetParent(colliders[0].transform);
		}
	}
}
