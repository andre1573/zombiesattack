using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class PlayerLobbyInfo : MonoBehaviourPunCallbacks
{

    [SerializeField] TMP_Text txPlayername;
	[SerializeField] SpriteRenderer imgPersonagem3D;
	[SerializeField] Image imgPersonagem;
	[SerializeField] Slider volumeVoice;
	[SerializeField] TMP_Text pontuacao;
	Player player;

    public void SetUp(Player _player)
    {
        player = _player;
		txPlayername.text = _player.NickName;
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
		{
			Debug.Log("Id1: "+ _player.UserId+" Id2: "+ player.GetComponent<PhotonView>().Owner.UserId);
			Debug.Log("Retorno iguais: " + _player.UserId == player.GetComponent<PhotonView>().Owner.UserId);
			if (_player.UserId == player.GetComponent<PhotonView>().Owner.UserId)
			{
				if(imgPersonagem3D!=null) imgPersonagem3D.sprite = player.GetComponent<PlayerController>().imgPersonagem;
				if (imgPersonagem!=null) imgPersonagem.sprite = player.GetComponent<PlayerController>().imgPersonagem;
				if (volumeVoice!=null) volumeVoice.value = player.GetComponent<AudioSource>().volume;
				if(pontuacao!=null) pontuacao.text = player.GetComponent<PlayerController>().gameController.txPontuacao.text;
				break;
			}
		}
    }
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		if (player == otherPlayer)
		{
			Destroy(gameObject);
		}
	}
	public override void OnLeftRoom()
	{
		Destroy(gameObject);
	}

	public void setVolume(float volume)
	{
		foreach(GameObject objPlayer in GameObject.FindGameObjectsWithTag("Player"))
        {
			if(player.UserId == objPlayer.GetComponent<PhotonView>().Owner.UserId)
            {
				objPlayer.GetComponent<AudioSource>().volume = volume;
				break;
			}
        }
	}

}
