using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.IO;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerController : MonoBehaviourPunCallbacks, IDamageable
{// lembrete: nome de usuarios iguais buga a mudança de cena

	[SerializeField] Image healthbarImage;
	[SerializeField] GameObject HUDCanvas;
	[SerializeField] Item[] items;
	
	public bool isMorto = false;
	public bool isZombie = false;
	[SerializeField] GameObject skin;
	[SerializeField] GameObject cabecaPivot;
	[SerializeField] GameObject cameraPivotFixo;
	[SerializeField] public Sprite imgPersonagem;

	int itemIndex;
	int previousItemIndex = -1;
	public const float maxHealth = 100f;
	float currentHealth = maxHealth;

	
	private float horizontalMove, verticalMove;
	public float walkingSpeed = 2.0f;
	public float runningSpeed = 5.0f;
	public float jumpSpeed = 8.0f;
	public bool isPulando = false;
	public float gravity = 20.0f;
	public Camera playerCamera;
	public GameObject cameraPivot;
	public float sensivity = 2.0f;
	public float lookXLimit = 45.0f;
	public float intensityFlashlight = 3.0f;
	public AudioSource somPassos;

	public CharacterController characterController;
	Vector3 moveDirection = Vector3.zero;
	float rotationX = 0;

	[HideInInspector]
	public bool canMove = true, isMenuCanvasAberto = false;

	public PhotonView PV;
	public Animator anim;
	PlayerManager playerManager;

	//cursor
	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;

	private float leftTimeDie = 4, timeDieMax = 4;
	private bool isTimeDieOn = false;
	public GameController gameController;

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);

		characterController = GetComponent<CharacterController>();
		PV = GetComponent<PhotonView>();
		anim = GetComponent<Animator>();
		if (PV != null) playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>(); //se pa tirar isso

		GameObject gc = GameObject.FindGameObjectWithTag("GameController");
		if (gc != null) gameController = gc.GetComponent<GameController>();

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void Start()
	{
		if (PV == null) return;
		if (PV.IsMine)
		{
			EquipItem(0);
			Cursor.lockState = CursorLockMode.Locked;
			//Cursor.visible = true;
			OnMouseEnter();
		}
		else
		{
			Destroy(GetComponentInChildren<Camera>().gameObject);
			Destroy(HUDCanvas);
			Destroy(characterController);
		}
	}

	void Update()
	{
		if (PV == null) return;
		if (!PV.IsMine)
			return;
		if (!gameController.isComecou) {
			gameController.isComecou = true;
			PV.RPC("RPC_RespawnarPlayer", RpcTarget.All, false);
		}
		if (isTimeDieOn)
		{
			canMove = false;
			if (leftTimeDie < 0)
			{
				canMove = true;
				leftTimeDie = timeDieMax;
				isTimeDieOn = false;
			}
			leftTimeDie -= Time.deltaTime;
		}

		if (Input.GetButtonDown("Fire1"))
		{
            if (isZombie)
            {
                if (items[itemIndex] != null)
                {
					items[itemIndex]?.Use();
					anim.SetBool("batendo", true);
				}
				
			}
            else
            {
				if (items[itemIndex] != null)
				{
					items[itemIndex]?.Use();
					anim.SetBool("atirando", true);
				}
			}
        }
        else if(Input.GetButtonUp("Fire1"))
        {
            if (isZombie)
            {
				anim.SetBool("batendo", false);
			}
            else
            {
				anim.SetBool("atirando", false);
			}
		}

		for (int i = 0; i < items.Length; i++)
		{
			if (Input.GetKeyDown((i + 1).ToString()))
			{
				EquipItem(i);
				break;
			}
		}

		if (Input.GetAxisRaw("Mouse ScrollWheel") > 0f)
		{
			if (itemIndex >= items.Length - 1)
			{
				EquipItem(0);
			}
			else
			{
				EquipItem(itemIndex + 1);
			}
		}
		else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0f)
		{
			if (itemIndex <= 0)
			{
				EquipItem(items.Length - 1);
			}
			else
			{
				EquipItem(itemIndex - 1);
			}
		}

		if (transform.position.y < -40f) // Die if you fall out of the world
		{
			Die();
		}

		characterController.Move(moveDirection * Time.deltaTime);
		Move();

		if (!characterController.isGrounded)
		{
			anim.SetBool("pulando", true);
			isPulando = true;
		}
		else
		{
			anim.SetBool("pulando", false);
			isPulando = false;
		}

		if(!isMorto && (characterController.velocity.x > 0 || characterController.velocity.z > 0) && characterController.isGrounded)
        {
			if(!somPassos.isPlaying) somPassos.Play();
        }
        else
        {
			somPassos.Stop();
        }

	}

	void EquipItem(int _index)
	{
		if (_index == previousItemIndex)
			return;

		itemIndex = _index;

		items[itemIndex].itemGameObject.SetActive(true);

		if (previousItemIndex != -1)
		{
			items[previousItemIndex].itemGameObject.SetActive(false);
		}

		previousItemIndex = itemIndex;

		if (PV.IsMine)
		{
			Hashtable hash = new Hashtable();
			hash.Add("itemIndex", itemIndex);
			PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
		}
	}

	public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
	{
		if (!PV.IsMine && targetPlayer == PV.Owner)
		{
			EquipItem((int)changedProps["itemIndex"]);
		}
	}

	void OnMouseEnter()
	{
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
	}

	public void TakeDamage(float damage)
	{
		PV.RPC("RPC_TakeDamage", RpcTarget.All, damage);
	}

	[PunRPC]
	void RPC_TakeDamage(float damage)
	{
		if (!PV.IsMine)
			return;

		currentHealth -= damage;

		healthbarImage.fillAmount = currentHealth / maxHealth;

		if (currentHealth <= 0)
		{
			Die();
		}
	}
	void Die()
	{
		if (isMorto) return;

		//criar instancia de personagem morto
		//mudar visao da camera branca
		GameObject playerDie;
		if (PlayerPrefs.GetInt("personagemskin") == 1)
		{
			playerDie = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PrefabElisabethDie"), transform.position, transform.rotation, 0, new object[] { PV.ViewID });
		}
		else if (PlayerPrefs.GetInt("personagemskin") == 2)
		{
			playerDie = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PrefabJamesDie"), transform.position, transform.rotation, 0, new object[] { PV.ViewID });
		}
		else if (PlayerPrefs.GetInt("personagemskin") == 3)
		{
			playerDie = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PrefabSophiaDie"), transform.position, transform.rotation, 0, new object[] { PV.ViewID });
		}
		else
		{
			playerDie = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PrefabBryceDie"), transform.position, transform.rotation, 0, new object[] { PV.ViewID });
		}

		PlayerPrefs.SetInt("scoreTotal", PlayerPrefs.GetInt("scoreTotal") - 10);
		isTimeDieOn = true;
		PV.RPC("RPC_ExecutarAcoesDie", RpcTarget.All);
	}


	void OnMouseExit()
	{
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
	}

	void Move()
	{
		// We are grounded, so recalculate move direction based on axes
		Vector3 forward = transform.TransformDirection(Vector3.forward);
		Vector3 right = transform.TransformDirection(Vector3.right);
		// Press Left Shift to run
		bool isRunning = Input.GetKey(KeyCode.LeftShift);
		float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
		float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
		float movementDirectionY = moveDirection.y;
		moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        if (canMove)
        {
			horizontalMove = Input.GetAxisRaw("Horizontal");
			verticalMove = Input.GetAxisRaw("Vertical");
            if (!Input.GetButtonDown("Fire1"))
            {
				anim.SetFloat("horizontalMove", horizontalMove);
				anim.SetFloat("verticalMove", verticalMove);
			}
		}

       /* if ((Input.GetButton("Horizontal") || Input.GetButton("Vertical")) && canMove) //se esta se movendo >> cancela as animacoes secundarias pra andar
        {
            if (isZombie)
            {
				anim.SetBool("batendo", false);
			}
            else
            {
				anim.SetBool("atirando", false);
			}
		}*/

		if (Input.GetKey(KeyCode.LeftControl) && canMove && characterController.isGrounded)
		{
			anim.SetBool("agachando", true);
			curSpeedX = 1.0f;
			curSpeedY = 1.0f;
		}
        else
        {
			anim.SetBool("agachando", false);
		}
		if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
		{
			moveDirection.y = jumpSpeed;
		}
		else
		{
			moveDirection.y = movementDirectionY; 
		}
		
		// Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
		// when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
		// as an acceleration (ms^-2)
		if (!characterController.isGrounded)
		{
			moveDirection.y -= gravity * Time.deltaTime;
		}

		// Player and Camera rotation
		if (canMove)
		{
			rotationX += -Input.GetAxis("Mouse Y") * sensivity;
			rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
			cameraPivot.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
			transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * sensivity, 0);
		}
		cameraPivot.transform.position = new Vector3(cameraPivotFixo.transform.position.x, cabecaPivot.transform.position.y, cabecaPivot.transform.position.z);
	}

	void FixedUpdate() //testar update ao inves de fixed
	{
		if (PV == null) return;
		if (!PV.IsMine)
			return;

		if (PlayerPrefs.GetFloat("sensivity") <= 0) sensivity = 2.0f;
		else sensivity = PlayerPrefs.GetFloat("sensivity");

	}

	[PunRPC]
	void RPC_ExecutarAcoesDie()
	{
		isMorto = true;
		skin.SetActive(false);
		GetComponent<AudioSource>().mute = true;
		//implementar morte
	}

	[PunRPC]
	void RPC_RespawnarPlayer(bool isLoading)
    {
		//gameController.checkpoint = null;
		gameController.isLoading = isLoading;
		PhotonNetwork.Destroy(gameObject);
	}

}