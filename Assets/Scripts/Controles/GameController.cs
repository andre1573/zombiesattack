using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class GameController : MonoBehaviourPunCallbacks
{

    public int qtdMaxEvidencias = 15;
    public Font fonte;
    [HideInInspector] public PhotonView PV;
    public GameObject btRestartGame, btReturnToLobby;
    public TMP_Text txPontuacao, txPlayername;
    [HideInInspector] public bool possuiPlayer, isLoading, isComecou;
    public GameObject checkpoint;
    

    void Awake() //se pa botar no awake
    {
        isLoading = false;
        PV = GetComponent<PhotonView>();
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
           PhotonNetwork.Destroy(player);
        }
        if (txPlayername != null)
        {
            txPlayername.text = PlayerPrefs.GetString("nickname");
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (btRestartGame != null) btRestartGame.SetActive(true);
            if (btReturnToLobby != null) btReturnToLobby.SetActive(true);
        }
        else
        {
            if (btRestartGame != null) btRestartGame.SetActive(false);
            if (btReturnToLobby != null) btReturnToLobby.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (txPontuacao != null)
        {
            if (PlayerPrefs.GetInt("idiomaIndex") == 0) txPontuacao.text = "Pontuação: " + PlayerPrefs.GetInt("scoreTotal");
            else txPontuacao.text = "Score: " + PlayerPrefs.GetInt("scoreTotal");
        }

       /* possuiPlayer = false;
        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (p!=null && p.GetComponent<PhotonView>()!=null && p.GetComponent<PhotonView>().IsMine)
            {
                possuiPlayer = true;
                break;
            }
        }
        if(!possuiPlayer && PhotonNetwork.InRoom && !isLoading) PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerManager"), Vector3.zero, Quaternion.identity);*/
    }


    [PunRPC]
    public void RPC_EndGame()
    {
        PhotonNetwork.LoadLevel(2);
    }

    [PunRPC]
    public void RestartGame() 
    {
        if (!PhotonNetwork.IsMasterClient) return;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.GetComponent<PlayerController>().PV.RPC("RPC_RespawnarPlayer", RpcTarget.All, true);
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PhotonNetwork.DestroyAll();
        PhotonNetwork.LoadLevel(3);
    }

    [PunRPC]
    public void RespawnPlayer()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                PlayerPrefs.SetInt("scoreTotal", PlayerPrefs.GetInt("scoreTotal") - 10);
                PhotonNetwork.Destroy(player);
            }
        }
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerManager"), Vector3.zero, Quaternion.identity);
    }

    public void VoltarAoMenuPrincipal() 
    {
        if (PhotonNetwork.OfflineMode)
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (player.GetComponent<PhotonView>().IsMine)
                {
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = true;
                    PhotonNetwork.Destroy(player);
                }
            }
            PhotonNetwork.LoadLevel(0);
        }
        else
        {
            PhotonNetwork.LoadLevel(0);
        }
    }
    public void VoltarAoLobby() 
    {
        if (!PhotonNetwork.IsMasterClient) return;
        PV.RPC("RPC_VoltarAoLobby", RpcTarget.All);
    }

    [PunRPC]
    public void RPC_VoltarAoLobby()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                PhotonNetwork.Destroy(player);
            }
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PhotonNetwork.LoadLevel(2);
    }

  
}
