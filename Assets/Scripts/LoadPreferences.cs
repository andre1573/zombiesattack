using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;
using Photon.Pun;
//using Steamworks;

public class LoadPreferences : MonoBehaviourPunCallbacks
{
    public AudioMixer audioMixer;
    public TMP_InputField nickname;

    // Start is called before the first frame update
    void Start()
    {
        audioMixer.SetFloat("volume", PlayerPrefs.GetFloat("volume"));
        Debug.Log("Grafico index = " + PlayerPrefs.GetInt("graphicsIndex"));
        if (PlayerPrefs.GetInt("graphicsIndex") == 0) PlayerPrefs.SetInt("graphicsIndex", 3);
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("graphicsIndex"));
        Screen.fullScreen = PlayerPrefs.GetInt("fullscreen") == 0 ? true : false;
        if(nickname!=null) nickname.text = PlayerPrefs.GetString("nickname") != null && PlayerPrefs.GetString("nickname") != "" ? PlayerPrefs.GetString("nickname") : "Player " + Random.Range(0, 1000).ToString("0000");
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        /*if (SteamApps.GetCurrentGameLanguage() == "brazilian" || SteamApps.GetCurrentGameLanguage() == "portuguese") PlayerPrefs.SetInt("idiomaIndex", 0);
        else PlayerPrefs.SetInt("idiomaIndex", 1);*/

    }

    // Update is called once per frame


    public void SetNickname(string name)
    {
        nickname.text = name;
        PlayerPrefs.SetString("nickname", name);
        PhotonNetwork.NickName = name;
    }

}
