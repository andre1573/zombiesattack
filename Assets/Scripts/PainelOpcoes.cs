using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;

public class PainelOpcoes : MonoBehaviourPunCallbacks
{

    bool open = false;
    public GameObject canvas;
    public bool isMenuPrincipal = false;

    // Update is called once per frame
    void Update()
    {
        if (!isMenuPrincipal && Input.GetKeyDown(KeyCode.Escape))
        {
            if (!open) AbrirMenu();
            else FecharMenu();
        }
    }

    public void FecharMenu()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                player.GetComponent<FirstPersonController>().canMove = true;
                break;
            }
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        open = false;
        canvas.SetActive(false);
    }

    public void AbrirMenu()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<PhotonView>().IsMine)
            {
                player.GetComponent<FirstPersonController>().canMove = false;
                break;
            }
        }
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        open = true;
        canvas.SetActive(true);
    }

    public void startOffline()
    {
       /* if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }*/
        PhotonNetwork.OfflineMode = true;
        PhotonNetwork.LoadLevel(1);
    }

    public void startOnline()
    {
       
        PhotonNetwork.OfflineMode = false;
       /* if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }*/
        PhotonNetwork.LoadLevel(1);
    }

}
