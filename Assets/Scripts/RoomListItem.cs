﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour
{
	[SerializeField] TMP_Text text;
	[SerializeField] TMP_InputField inputPassword;
	[SerializeField] TMP_Text msgAviso, qtdPlayersAtual;
	[SerializeField] Button btEntrar;

	public RoomInfo info;

	public string password;

	public void SetUp(RoomInfo _info)
	{
		info = _info;
		int posicao = _info.Name.IndexOf("#.#.#");
		text.text = _info.Name.Substring(0, posicao);
		password = _info.Name.Substring(posicao + 5, Mathf.Abs(posicao + 5 - _info.Name.Length));
		qtdPlayersAtual.text = _info.PlayerCount + "/" + _info.MaxPlayers;
		if (_info.PlayerCount == 4)
		{
			qtdPlayersAtual.color = Color.red;
			btEntrar.interactable = false;
		}
	}

	public void OnClick()
	{
		if(info.PlayerCount >= info.MaxPlayers)
        {
			if (PlayerPrefs.GetInt("idiomaIndex") == 0) msgAviso.text = "A sala está cheia!";
			else msgAviso.text = "The room is full!";
			return;
		}
		if(password == null || password == "" || inputPassword.text == password)
        {
			Launcher.Instance.JoinRoom(info);
        }
        else
        {
			msgAviso.color = Color.red;
			if (PlayerPrefs.GetInt("idiomaIndex") == 0) msgAviso.text = "Senha incorreta";
			else msgAviso.text = "Incorrect password";

		}
		
	}
}