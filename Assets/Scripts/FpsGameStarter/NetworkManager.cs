using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class NetworkManager : MonoBehaviourPunCallbacks, IPunObservable {

    [SerializeField]
    private Text connectionText;
    [SerializeField]
    private Transform[] spawnPoints;
    [SerializeField]
    private Camera sceneCamera;
    [SerializeField]
    private GameObject serverWindow;
    [SerializeField]
    private GameObject messageWindow;
    [SerializeField]
    private GameObject sightImage;
    [SerializeField]
    private InputField username;
    [SerializeField]
    private InputField roomName;
    [SerializeField]
    private InputField roomList;
    [SerializeField]
    private InputField messagesLog; 

    private GameObject player;
    private Queue<string> messages;
    private const int messageCount = 10;
    private string nickNamePrefKey = "PlayerName";

    [HideInInspector] public bool isPlaying = false, isSpawnou = false, transformouEmZombies = false;
    public int minPlayersForStart = 2, qtdHumanos=0, qtdZombies=0;
    private float timeDelayStart = 5, timeLeft;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start() {
        messages = new Queue<string> (messageCount);
        if (PlayerPrefs.HasKey(nickNamePrefKey)) {
            username.text = PlayerPrefs.GetString(nickNamePrefKey);
        }
       // RespawnHumano(0.0f);
        isPlaying = false;
        isSpawnou = false;
        transformouEmZombies = false;
    }

    /// <summary>
    /// Called on the client when you have successfully connected to a master server.
    /// </summary>
    public override void OnConnectedToMaster() {
        PhotonNetwork.JoinLobby();
    }

    /// <summary>
    /// Called on the client when the connection was lost or you disconnected from the server.
    /// </summary>
    /// <param name="cause">DisconnectCause data associated with this disconnect.</param>
    public override void OnDisconnected(DisconnectCause cause) {
        connectionText.text = cause.ToString();
    }

    /// <summary>
    /// Callback function on joined lobby.
    /// </summary>
    public override void OnJoinedLobby() {
        serverWindow.SetActive(true);
        connectionText.text = "";
    }

    /// <summary>
    /// Callback function on reveived room list update.
    /// </summary>
    /// <param name="rooms">List of RoomInfo.</param>
    public override void OnRoomListUpdate(List<RoomInfo> rooms) {
        roomList.text = "";
        foreach (RoomInfo room in rooms) {
            roomList.text += room.Name + "\n";
        }
    }

    /// <summary>
    /// Callback function on joined room.
    /// </summary>
    public override void OnJoinedRoom() {
        connectionText.text = "";
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
       
    }

    /// <summary>
    /// Start spawn or respawn a player.
    /// </summary>
    /// <param name="spawnTime">Time waited before spawn a player.</param>
    void RespawnHumano(float spawnTime) {
        sightImage.SetActive(false);
        sceneCamera.enabled = true;
        StartCoroutine(RespawnCoroutine(spawnTime));
    }

    /// <summary>
    /// The coroutine function to spawn player.
    /// </summary>
    /// <param name="spawnTime">Time waited before spawn a player.</param>
    IEnumerator RespawnCoroutine(float spawnTime) {
        yield return new WaitForSeconds(spawnTime);
        messageWindow.SetActive(true);
        sightImage.SetActive(true);
        int spawnIndex = Random.Range(0, spawnPoints.Length);
        player = InstanciarPlayer(spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation, true); //instancia o player no modo humano
        PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
        playerHealth.RespawnEvent += RespawnHumano;
        playerHealth.AddMessageEvent += AddMessage;
        sceneCamera.enabled = false;
        if (spawnTime == 0) {
            AddMessage("Player " + PhotonNetwork.LocalPlayer.NickName + " Joined Game.");
        } else {
            AddMessage("Player " + PhotonNetwork.LocalPlayer.NickName + " Respawned.");
        }
    }

    public GameObject InstanciarPlayer(Vector3 position, Quaternion rotation, bool isHumano)
    {
        foreach (GameObject objPlayer in GameObject.FindGameObjectsWithTag("Player")){
            if(objPlayer.GetComponent<PhotonView>().IsMine) PhotonNetwork.Destroy(objPlayer);
        }
        if (isHumano)
        {
             if (PlayerPrefs.GetInt("skinhumano") == 1)
            {
                return PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerHumano1"), position, rotation, 0, new object[] { photonView.ViewID });
            }
            else //skinhumano == 0
            {
                return PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerHumano0"), position, rotation, 0, new object[] { photonView.ViewID });
            }
        }
        else
        {
            if (PlayerPrefs.GetInt("skinzombie") == 1)
            {
                return PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerZombie1"), position, rotation, 0, new object[] { photonView.ViewID });
            }
            else //skinzombie == 0
            {
                return PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerZombie0"), position, rotation, 0, new object[] { photonView.ViewID });
            }
        }
       
    }

    /// <summary>
    /// Add message to message panel.
    /// </summary>
    /// <param name="message">The message that we want to add.</param>
    void AddMessage(string message) {
        photonView.RPC("AddMessage_RPC", RpcTarget.All, message);
    }

    /// <summary>
    /// RPC function to call add message for each client.
    /// </summary>
    /// <param name="message">The message that we want to add.</param>
    [PunRPC]
    void AddMessage_RPC(string message) {
        messages.Enqueue(message);
        if (messages.Count > messageCount) {
            messages.Dequeue();
        }
        messagesLog.text = "";
        foreach (string m in messages) {
            messagesLog.text += m + "\n";
        }
    }

    /// <summary>
    /// Callback function when other player disconnected.
    /// </summary>
    public override void OnPlayerLeftRoom(Player other) {
        if (PhotonNetwork.IsMasterClient) {
            AddMessage("Player " + other.NickName + " Left Game.");
        }
    }

    /// <summary>
    /// Used to customize synchronization of variables in a script watched by a photon network view.
    /// </summary>
    /// <param name="stream">The network bit stream.</param>
    /// <param name="info">The network message information.</param>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(messagesLog.text);
        }
        else
        {
            messagesLog.text = (string)stream.ReceiveNext();
        }
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if (isPlaying) //partida está rolando
        {
            if (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
            }
            else
            {
                if (!transformouEmZombies) transformarPlayersEmZombies();
                if (qtdHumanos <= 0) //zombies win
                {
                    acoesZombiesWin();
                    zerarAtributosPorRound();
                }
                else if (qtdZombies <= 0) //humanos win
                {
                    acoesHumanosWin();
                    zerarAtributosPorRound();
                }
            }
        }
        else //partida nao começou
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount >= minPlayersForStart)
            {
                isPlaying = true;
                timeLeft = timeDelayStart;
            }
            if (!isSpawnou) { //renasce os players
                photonView.RPC("RPC_RespawnarPlayers", RpcTarget.All);
            }
        }
    }

    private void zerarAtributosPorRound()
    {
        isPlaying = false;
        transformouEmZombies = false;
        qtdHumanos = PhotonNetwork.CurrentRoom.PlayerCount;
        qtdZombies = 0;
    }

    private void transformarPlayersEmZombies()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        int qtdPlayersZombies = PhotonNetwork.CurrentRoom.PlayerCount/2 ; //2
        Debug.Log("Transformando em zombies");
        while (qtdZombies < qtdPlayersZombies) { 
            foreach(GameObject objPlayer in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (!objPlayer.GetComponent<PlayerNetworkMover>().isZombie)
                {
                    if (1 == Random.Range(1, 3))
                    {
                        photonView.RPC("RPC_TransformarEmZombie", RpcTarget.All, objPlayer.GetComponent<PhotonView>().Owner);
                        qtdZombies++;
                    }
                }
                if (qtdZombies >= qtdPlayersZombies) break; 
            }
        }
        qtdHumanos = PhotonNetwork.CurrentRoom.PlayerCount - qtdZombies;
        transformouEmZombies = true;
    }

    [PunRPC]
    private void RPC_TransformarEmZombie(Player owner)
    {
        foreach (GameObject objPlayer in GameObject.FindGameObjectsWithTag("Player")){
            if(objPlayer.GetComponent<PhotonView>().Owner == owner && PhotonNetwork.LocalPlayer == owner)
            {
                InstanciarPlayer(objPlayer.transform.position, objPlayer.transform.rotation, false);
                Debug.Log("Transformou em zombie");
                return;
            }
        }
    }

    [PunRPC]
    private void RPC_RespawnarPlayers()
    {
        isSpawnou = true;
        int spawnIndex = Random.Range(0, spawnPoints.Length);
        InstanciarPlayer(spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation, true); //instancia o player no modo humano
    }

    private void acoesHumanosWin()
    {
        Debug.Log("HUMANOS WIN");
        photonView.RPC("RPC_DestruirPlayers", RpcTarget.All);
    }

    private void acoesZombiesWin()
    {
        Debug.Log("ZOMBIES WIN");
        photonView.RPC("RPC_DestruirPlayers", RpcTarget.All);
    }

    [PunRPC]
    private void RPC_DestruirPlayers()
    {
        isSpawnou = false;
        foreach (GameObject objPlayer in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (objPlayer.GetComponent<PhotonView>().Owner == PhotonNetwork.LocalPlayer)
            {
                PhotonNetwork.Destroy(objPlayer);
                return;
            }
        }
    }

}
