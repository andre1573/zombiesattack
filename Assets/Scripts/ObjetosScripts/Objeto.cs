using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

[RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(PhotonView)), RequireComponent(typeof(PhotonRigidbodyView))]
public class Objeto : MonoBehaviourPunCallbacks
{
    public PhotonView PV;

    void Start()
    {
        gameObject.tag = "objeto";
        PV = GetComponent<PhotonView>();
    }

}
