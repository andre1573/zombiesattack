using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class CameraPreLoad : MonoBehaviourPunCallbacks
{

    // Update is called once per frame
    void Update()
    {
       GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
       foreach (GameObject p in players)
       {
           if (p.GetComponent<PhotonView>().IsMine)
           {
               Destroy(gameObject);
               break;
           }
        }
    }
}
