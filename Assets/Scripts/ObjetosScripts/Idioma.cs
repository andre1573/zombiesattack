using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Idioma : MonoBehaviour
{

    public Text singleplayer, multiplayer, settings, settings2, quit, confirm, idioma, resolution, quality, fullscreen, sensivity, volumeVoice;
    public Text respawn, restartGame, returnLobby;
    public Text loading, findRoom, createRoom, mainMenu, criarSala, startGame, startGame2, leaveRoom, findRoom2, voltar, continuar;
    public TMP_Text placeholderNickname, placeholderRoomName, placeholderRoomPassword, entrar, placeholderPassword, confirmar, cancelar, filtrar;
    public Text controles, txW, txA, txS, txD, txCtrl, txCorrer, txEspaco, txMouseDir, txMouseEsq, txF;

    // Start is called before the first frame update
    void Start()
    {
        alterarIdioma();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void alterarIdioma()
    {
        if (PlayerPrefs.GetInt("idiomaIndex") == 0) //portugues
        {
            if(singleplayer!=null) singleplayer.text = "Jogar sozinho";
            if (multiplayer != null) multiplayer.text = "Jogar online";
            if (settings != null) settings.text = "Ajustes";
            if (settings2 != null) settings2.text = "Ajustes";
            if (quit != null) quit.text = "Sair";
            if (confirm != null) confirm.text = "Confirmar";
            if (idioma != null) idioma.text = "Idioma";
            if (resolution != null) resolution.text = "Resolu��o";
            if (quality != null) quality.text = "Qualidade";
            if (fullscreen != null) fullscreen.text = "Tela cheia";
            if (sensivity != null) sensivity.text = "Sensibilidade";
            if (volumeVoice != null) volumeVoice.text = "Volume das vozes";

            if (loading != null) loading.text = "Carregando...";
            if (placeholderNickname != null) placeholderNickname.text = "Insira seu apelido...";
            if (findRoom != null) findRoom.text = "Buscar sala";
            if (createRoom != null) createRoom.text = "Criar sala";
            if (mainMenu != null) mainMenu.text = "Menu principal";
            if (placeholderRoomName != null) placeholderRoomName.text = "Insira o nome da sala...";
            if (placeholderRoomPassword != null) placeholderRoomPassword.text = "Insira a senha da sala...";
            if (criarSala != null) criarSala.text = "Criar sala";
            if (startGame != null) startGame.text = "Iniciar jogo";
            if (startGame2 != null) startGame2.text = "Iniciar jogo";
            if (leaveRoom != null) leaveRoom.text = "Sair da sala";
            if (findRoom2 != null) findRoom2.text = "Buscar sala";
            if (voltar != null) voltar.text = "Voltar";
            if (entrar != null) entrar.text = "Entrar";
            if (placeholderPassword != null) placeholderPassword.text = "Digite a senha...";
            if (continuar != null) continuar.text = "Continuar";

            if (respawn != null) respawn.text = "Renascer";
            if (restartGame != null) restartGame.text = "Reiniciar";
            if (returnLobby != null) returnLobby.text = "Voltar ao sagu�o";

            if (controles != null) controles.text = "Controles";
            if (txW != null) txW.text = "Andar pra frente: W";
            if (txA != null) txA.text = "Andar pra esquerda: A";
            if (txS != null) txS.text = "Andar pra tr�s: S";
            if (txD != null) txD.text = "Andar pra direita: D";
            if (txCtrl != null) txCtrl.text = "Agachar: Ctrl";
            if (txCorrer != null) txCorrer.text = "Correr: Shift";
            if (txEspaco != null) txEspaco.text = "Pular: Espa�o";
            if (txMouseDir != null) txMouseDir.text = "Interagir: Mouse esquerdo";
            if (txMouseEsq != null) txMouseEsq.text = "Fechar papel: Mouse direito";
            if (txF != null) txF.text = "Ligar/Desligar Lanterna: F";

            if (confirmar != null) confirmar.text = "Confirmar";
            if (cancelar != null) cancelar.text = "Cancelar";
            if (filtrar != null) filtrar.text = "Filtrar";
        }
        else //ingles
        {
            if (singleplayer != null) singleplayer.text = "Singleplayer";
            if (multiplayer != null) multiplayer.text = "Multiplayer";
            if (settings != null) settings.text = "Settings";
            if (settings2 != null) settings2.text = "Settings";
            if (quit != null) quit.text = "Quit";
            if (confirm != null) confirm.text = "Confirm";
            if (idioma != null) idioma.text = "Language";
            if (resolution != null) resolution.text = "Resolution";
            if (quality != null) quality.text = "Quality";
            if (fullscreen != null) fullscreen.text = "Fullscreen";
            if (sensivity != null) sensivity.text = "Sensivity";
            if (volumeVoice != null) volumeVoice.text = "Voice Chat";

            if (loading != null) loading.text = "Loading...";
            if (placeholderNickname != null) placeholderNickname.text = "Insert your nickname...";
            if (findRoom != null) findRoom.text = "Find room";
            if (createRoom != null) createRoom.text = "Create room";
            if (mainMenu != null) mainMenu.text = "Main menu";
            if (placeholderRoomName != null) placeholderRoomName.text = "Enter room name...";
            if (placeholderRoomPassword != null) placeholderRoomPassword.text = "Enter room password...";
            if (criarSala != null) criarSala.text = "Create room";
            if (startGame != null) startGame.text = "Start game";
            if (startGame2 != null) startGame2.text = "Start game";
            if (leaveRoom != null) leaveRoom.text = "Leave room";
            if (findRoom2 != null) findRoom2.text = "Find room";
            if (voltar != null) voltar.text = "Back";
            if (entrar != null) entrar.text = "Enter";
            if (placeholderPassword != null) placeholderPassword.text = "Insert password...";
            if (continuar != null) continuar.text = "Continue";

            if (respawn != null) respawn.text = "Respawn";
            if (restartGame != null) restartGame.text = "Restart";
            if (returnLobby != null) returnLobby.text = "Return to lobby";

            if (controles != null) controles.text = "Controls";
            if (txW != null) txW.text = "Walk forward: W";
            if (txA != null) txA.text = "Walk left: A";
            if (txS != null) txS.text = "Walk back: S";
            if (txD != null) txD.text = "Walk right: D";
            if (txCtrl != null) txCtrl.text = "Crouch: Ctrl";
            if (txCorrer != null) txCorrer.text = "Run: Shift";
            if (txEspaco != null) txEspaco.text = "Jump: Espace";
            if (txMouseDir != null) txMouseDir.text = "Interact: Left Mouse";
            if (txMouseEsq != null) txMouseEsq.text = "Close paper: Right Mouse";
            if (txF != null) txF.text = "Turn On/Off Flashlight: F";

            if (confirmar != null) confirmar.text = "Confirm";
            if (cancelar != null) cancelar.text = "Cancel";
            if (filtrar != null) filtrar.text = "Apply";
        }
    }

}
