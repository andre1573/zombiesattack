using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextoTraducao : MonoBehaviour
{

    public TMP_Text texto;
    public string textoPortuges, textoIngles;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (PlayerPrefs.GetInt("idiomaIndex") == 0)
        { //portugues
            if (texto != null) texto.text = textoPortuges;
        }
        else //ingles
        {
            if (texto != null) texto.text = textoIngles;
        }
    }
}
